#!/usr/bin/env perl
#
# Perform a full backup of a Git repository.

$git_repos = "/Users/Jed/Dropbox/.repos/";
$backups_dir = "/Users/Jed/Backups/git";
$date = "." . `date +%Y%m%d%H%M%S`;
chomp $date;

my @folders;

opendir(DIR, $git_repos) or die "cant find $git_repos: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-d "$git_repos$file"){
			push @folders,"$file";
	}
}
closedir(DIR);

foreach my $repo (@folders) {
	$hash = `cd $git_repos$repo && git log -1 --pretty=format:%h && cd ~`;
	$previous = `find . -name "*.$hash.*"`;

	# print "$repo:\n";

	if ( ! $previous) {
		print `rsync -acE $git_repos$repo $backups_dir/$repo`;
		# print "Compressing dump file...\n";
		print `tar -pczf $backups_dir/$repo.$hash$date.tar.gz -C$backups_dir $repo`;
	}
}


# Clean out old backups

my @backups;

opendir(DIR, $backups_dir) or die "cant find $backups_dir: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-f "$backups_dir/$file"){
			push @backups,"$backups_dir/$file";
	}
}
closedir(DIR);

foreach my $bk (@backups){
	next unless -M $bk > 28;
  print "Deleting $bk... \n";
  unlink $bk or die "Failed to remove $bk: $!";
}
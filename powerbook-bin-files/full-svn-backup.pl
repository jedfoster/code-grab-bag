#!/usr/bin/env perl
#---
# Excerpted from "Pragmatic Guide to Subversion",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/pg_svn for more book information.
#---
# 
# Perform a full backup of a Subversion repository. 

$svn_repos = "/Users/Jed/svn/"; 
$backups_dir = "/Users/Jed/Backups/svn";


# Clean out old backups

my @backups;

opendir(DIR, $backups_dir) or die "cant find $backups_dir: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-f "$backups_dir/$file"){	# print "$backups_dir/$file\n";
			push @backups,"$backups_dir/$file";
	}
}
closedir(DIR);

foreach my $bk (@backups){
	next unless -M $bk > 28;
  print "Deleting $bk... \n";
  unlink $bk or die "Failed to remove $bk: $!";
}


# Backup repositories

my @folders;

opendir(DIR, $svn_repos) or die "cant find $svn_repos: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-d "$svn_repos$file"){
			push @folders,"$file";
	}
}
closedir(DIR);

foreach my $repo (@folders){
	$backup_file = "$repo.full." . `date +%Y%m%d%H%M%S`; 
	$youngest = `svnlook youngest $svn_repos$repo`; 
	chomp $youngest; 

	print "Backing up to revision $youngest\n"; 
	$svnadmin_cmd = "svnadmin dump --revision 0:$youngest " . 
	                "$svn_repos$repo > $backups_dir/$backup_file"; 
	`$svnadmin_cmd`; 

	print "Compressing dump file...\n"; 
	print `gzip -9 $backups_dir/$backup_file`; 

	open(LOG, ">$backups_dir/$repo.rev"); 
	print LOG $youngest; 
	close LOG;
}
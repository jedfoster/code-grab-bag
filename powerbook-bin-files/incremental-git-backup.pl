#!/usr/bin/env perl
# 
# Perform a full backup of a Git repository. 

$git_repos = "/Users/Jed/Dropbox/.repos/"; 
$backups_dir = "/Users/Jed/Backups/git";
$date = "." . `date +%Y%m%d%H%M%S`;
chomp $date;

my @folders;

opendir(DIR, $git_repos) or die "cant find $git_repos: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-d "$git_repos$file"){
			push @folders,"$file";
	}
}
closedir(DIR);

foreach my $repo (@folders){	
	print `rsync -acE $git_repos$repo $backups_dir/$repo`;
}

#!/usr/bin/env perl
#---
# Excerpted from "Pragmatic Guide to Subversion",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/pg_svn for more book information.
#---
# 
# Perform an incremental backup of a Subversion repository.

$svn_repos = "/Users/Jed/svn/"; 
$backups_dir = "/Users/Jed/Backups/svn";

my @folders;

opendir(DIR, $svn_repos) or die "cant find $svn_repos: $!";
while (defined(my $file = readdir(DIR))) {
	next if $file =~ /^\.\.?$/;
	if (-d "$svn_repos$file"){
			push @folders,"$file";
	}
}
closedir(DIR);

foreach my $repo (@folders){
	$backup_file = "$repo.incremental." . `date +%Y%m%d%H%M%S`; 

	open(IN, "$backups_dir/$repo.rev"); 
	$previous_youngest = <IN>; 
	chomp $previous_youngest; 
	close IN; 
	$youngest = `svnlook youngest $svn_repos$repo`; 
	chomp $youngest; 

	if($youngest eq $previous_youngest) { 
	  print "No new revisions to back up.\n"; 
	  # exit 0;
	}
	else {
		# We need to backup from the last backed up revision 
		# to the latest (youngest) revision in the repository 
		$first_rev = $previous_youngest + 1; 
		$last_rev = $youngest; 

		print "Backing up revisions $first_rev to $last_rev...\n"; 
		$svnadmin_cmd = "svnadmin dump --incremental " . 
		                "--revision $first_rev:$last_rev " . 
		                "$repo > $backups_dir/$backup_file"; 
		`$svnadmin_cmd`;
 
		print "Compressing dump file...\n"; 
		print `gzip -9 $backups_dir/$backup_file`;
 
		open(LOG, ">$backups_dir/$repo.rev"); 
		print LOG $last_rev; 
		close LOG;
	}
}
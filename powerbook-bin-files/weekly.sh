#!/usr/bin/env bash

. /Users/Jed/.bashrc

# Backup the Git repos
full-git-backup.pl

# Backup the SVN repos
full-svn-backup.pl

# Send everything to Delta
rsync -e "ssh -i /Users/Jed/.ssh/rsync" -acE --delete-after --filter='P sql' --rsync-path=/usr/bin/rsync  ~/Backups/ jed@mini.local:/Volumes/Delta/Backups